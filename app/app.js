'use strict';

// Declare app level module which depends on views, and components
var MathApp = angular.module('MathApp', [
    'ngRoute',
    'MathApp.Questions'
    ])
    .config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
        $routeProvider
            .when('/quiz/:quizType', {
                templateUrl: 'math/quiz.html',
                controller: 'QuestionsController',
            })
            .otherwise({redirectTo: '/quiz/add'});
        //$locationProvider.html5Mode(true);
    }]);
