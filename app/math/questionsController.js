/**
 * Created by didym_000 on 2015-10-19.
 */
var Questions = angular.module('MathApp.Questions', []);

Questions.controller('QuestionsController', ['$scope', '$log', '$routeParams',
    function($scope, $log, $routeParams) {
        $scope.minRange = 1;
        $scope.maxRange = 12;
        $scope.panelClass = 'panel-default';
        $scope.type = $routeParams.quizType;
        $scope.correct = 0;
        $scope.total = 0;

        var randomize = function() {
            if ($scope.useDecimals) {
                return Math.floor(Math.random() * ($scope.maxRange - $scope.minRange + 1) * 100 + $scope.minRange * 100) / 100;
            }
            return Math.floor(Math.random() * ($scope.maxRange - $scope.minRange + 1) + $scope.minRange);
        };

        var generateNumbers = function() {
            $scope.first = randomize();
            $scope.second = randomize();
        };

        var makeAddition = function() {
            generateNumbers();
            $scope.answer = $scope.first + $scope.second;
            $log.debug($scope.first, $scope.second, $scope.answer);
        };

        var makeSubtraction = function() {
            generateNumbers();
            if (! $scope.allowNegative && $scope.first < $scope.second) {
                var tmp = $scope.second;
                $scope.second = $scope.first;
                $scope.first = tmp;
            }
            $scope.answer = $scope.first - $scope.second;
        };

        var makeMultiplication = function() {
            generateNumbers();
            $scope.answer = $scope.first * $scope.second;
        };

        var makeDivision = function() {
            $scope.answer = randomize();
            $scope.second = randomize();
            $scope.first = $scope.answer * $scope.second;
        };

        $scope.makeQuestion  = function() {

            switch ($scope.type) {
                case 'add':
                    makeAddition();
                    break;
                case 'subtract':
                    makeSubtraction();
                    break;
                case 'multiply':
                    makeMultiplication();
                    break;
                case 'divide':
                    makeDivision();
                    break;
            }
            $log.debug($scope.first, $scope.second, $scope.answer);
        };

        var setGoodAnswer = function() {
            $scope.correct++;
            $scope.isCorrect = true;
            $scope.panelClass = 'panel-success';
        };

        var setBadAnswer = function() {
            $scope.panelClass = 'panel-danger';
            $scope.isCorrect = false;
        };

        var resetQuestion = function() {
            $scope.total++;
            $scope.response = null;
        };

        $scope.checkAnswer = function() {
            if ($scope.response == $scope.answer) {
                setGoodAnswer();
            } else {
                setBadAnswer();
            }

            resetQuestion();
        };
}]);

Questions
    .filter('getSign', function() {
        return function(input) {
            switch(input) {
                case 'add':
                    return '+';
                case 'subtract':
                    return '-';
                case 'multiply':
                    return 'x';
                case 'divide':
                    return '/';
            }
        }
    })
    .filter('getOperation', function() {
        return function(input) {
            switch(input) {
                case 'add':
                    return 'addition';
                case 'subtract':
                    return 'subtraction';
                case 'multiply':
                    return 'multiplication';
                case 'divide':
                    return 'division';
            }
        }
    });