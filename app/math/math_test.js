/**
 * Created by didym_000 on 2015-10-19.
 */
describe('MathApp.Questions', function() {

    var $controller, MathService, $scope;

    beforeEach(module('MathApp'));
    beforeEach(module('MathApp.Questions'));

    beforeEach( inject( function(_$controller_, $rootScope) {
        $controller = _$controller_;
        $scope = $rootScope.$new();
    }));

    describe('QuestionsController', function() {
        beforeEach( inject( function() {
            $controller('QuestionsController', {
                $scope: $scope
            });
        }));

        // This is a main method
        describe('makeQuestion', function() {

            it('should exist', function() {
                expect($scope.makeQuestion).toBeDefined();
            });

            it('should set first, second, and answer numbers', function() {

                $scope.type = 'add';
                $scope.makeQuestion();

                expect($scope.first).toBeDefined();
                expect(typeof $scope.first).toEqual('number');

                expect($scope.second).toBeDefined();
                expect(typeof $scope.second).toEqual('number');

                expect($scope.answer).toBeDefined();
                expect(typeof $scope.answer).toEqual('number');

            });

            it('should multiply when quizType is set to "multiply"', function() {
                $scope.type = 'multiply';
                $scope.makeQuestion();
                var correct = $scope.first * $scope.second;
                expect($scope.answer).toEqual(correct);
            });

            it('should add when quizType is set to "add"', function() {
                $scope.type = 'add';
                $scope.makeQuestion();
                var correct = $scope.first + $scope.second;
                expect($scope.answer).toEqual(correct);
            });

            it('should substract when quizType is set to "substract"', function() {
                $scope.type = 'subtract';
                $scope.makeQuestion();
                var correct = $scope.first - $scope.second;
                expect($scope.answer).toEqual(correct);
            });

            it('should divide when quizType is set to "divide"', function() {
                $scope.type = 'divide';
                $scope.makeQuestion();
                var correct = $scope.first / $scope.second;
                expect($scope.answer).toEqual(correct);
            });

        });
    });
});